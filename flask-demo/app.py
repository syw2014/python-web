#!/usr/bin/env python
# Author: Jerry.Shi
# Date: 2017-08-10 14:49:30

from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    query = request.args.get('query')
    return render_template('index.html', query=query)
    #return request.args.get('query')

@app.route('/hello/<name>')
def hello(name):
    return render_template('index.html', name=name)

if __name__ == '__main__':
    app.run(debug=True, host='10.143.1.22')
