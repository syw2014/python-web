#!/usr/bin/env python
# coding: utf-8
# Author: Jerry.Shi
# Date: 2017-05-28 17:12:30

from wsgiref.simple_server import make_server
from controller.admin import *
from controller.account import *

"""
# user define operator
def index():
    return '<h1>Helix, index!</h1>'

def login():
    html = '''<p>UserName:<input /></p><p>Password:<input /></p>'''
    return html

def logout():
    return '<h1>Helix, logout!</h1>'
"""


url = (
        ('/index/', index),
        ('/login/', login),
        ('/logout/', logout)
        )

def RunServer(environ, start_response):
    start_response('200 OK', [('content-Type', 'text/html')])
    
    # get user input url path
    user_path = environ['PATH_INFO']
    print 'user input url path: %s' % user_path
    
    func = None

    for item in url:
        if item[0] == user_path:
            func = item[1]()    # call process function
            break
    if func == None:
        return '<h2>Helix, 404 page not found !</h2>'

    return func


if __name__ == '__main__':
    httpd = make_server('10.143.1.22', 8888, RunServer)
    print 'Start serving on port 8888 ...'
    httpd.serve_forever()
